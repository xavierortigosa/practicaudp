#include <iostream>
#include <SFML\Graphics.hpp>
#include <SFML\Network.hpp>
#include <thread>
#include <mutex>
#include <Windows.h>
#include <PlayerInfo.h>
#include <map>
#include <jdbc/mysql_connection.h>
#include <jdbc/mysql_driver.h>
#include <jdbc/cppconn/resultset.h>
#include <jdbc/cppconn/statement.h>

std::mutex mtx;


void pingFunction(sf::UdpSocket* sock, std::vector<clientProxy> &clients)
{
	sf::Packet pckPing;
	int idPacket;
	for (size_t i = 0; i < clients.size(); i++)
	{
		idPacket = i;
		pckPing << PING << idPacket;
		clients[i].numPings += 1;

		if (sock->send(pckPing, clients[i].ip, clients[i].port) != sf::Socket::Done)
		{
			std::cout << "errrrrrrrrrrrrrrrrrrrrrrrrrrrrrrror";
		}
		pckPing.clear();

	}
	std::cout << "ping";
	Sleep(1000);

}



bool findPlayer(std::vector<clientProxy> clients, clientProxy toCompare)
{
	for (size_t i = 0; i < clients.size(); i++)
	{
		if (clients[i].alias == toCompare.alias) return true;
	}
	return false;
};
int pckCounter;

void pckFunction(sf::UdpSocket* sock, std::vector<clientProxy> &clients, std::map<int, sf::Packet> &packetescriticos, std::vector<int> &toErase)
{
	while (true)
	{		
		for (int i = 0; i < toErase.size(); i++)
		{
			
			packetescriticos.erase(toErase[i]);
		}
		toErase.clear();

		mtx.lock();
		for (std::map<int, sf::Packet>::iterator it = packetescriticos.begin(); it != packetescriticos.end(); it++)
		{
			for (int i = 0; i < clients.size(); i++)
			{				
				sock->send(it->second, clients[i].ip, clients[i].port);				
			}
		}
		mtx.unlock();		
		
		Sleep(2000);
	}
}

void receiveFunction(sf::UdpSocket* sock, std::vector<clientProxy> &clients, std::map<int,sf::Packet> &packetescriticos, std::vector<int> &toErase)
{
	sf::Packet pckToReceive;
	sf::Packet pckToSend;
	AccumMovement accum;
	std::vector<AccumMovement>Movements;
	int commandToSend;
	int commandToReceive;
	int idMove;
	int DeltaX;
	int AcumX;
	int AcumY;

	int id_pck;
	
	
	while (true)
	{
		clientProxy temp;
		sf::UdpSocket::Status status = sock->receive(pckToReceive, temp.ip, temp.port);

		
		pckToReceive >> commandToReceive;
		std::cout << "cmd: " << commandToReceive << std::endl;

		if (status == sf::UdpSocket::Status::Done)
		{
			switch (commandToReceive)
			{
				//NUEVO CLIENTE
			case HELLO:
				//CREO UN CLIENTPROXY TEMPORAL
				pckToReceive >> temp.alias;
				pckToReceive >> temp.pos.x;
				pckToReceive >> temp.pos.y;
				temp.numPings = 0;
				//WELCOME
				if (!findPlayer(clients, temp)) 
				{
					temp.pos = { temp.pos.x,  temp.pos.y };
					clients.push_back(temp);

					std::cout << temp.alias << std::endl;
					//SUBO AL VECTOR DE CLIENTES EL NUEVO

					for (int i = 0; i < (clients.size() - 1); i++)
					{
						commandToSend = WELCOME;
						pckToSend << pckCounter << commandToSend << temp.alias<< temp.pos.x << temp.pos.y;
						packetescriticos[pckCounter] = pckToSend;
						pckCounter++;
						//std::cout << "Al jugador con alias " << clients[i].alias << " le envio el jugador con alias " << temp.alias << std::endl;
						if (sock->send(pckToSend, clients[i].ip, clients[i].port) != sf::Socket::Done)
						{
							std::cout << "errrrrrrrrrrrrrrrrrrrrrrrrrrrrrrror";
						}
						pckToSend.clear();
					}

					for (int i = 0; i < (clients.size() - 1); i++)
					{
						commandToSend = WELCOME;
						pckToSend << pckCounter << commandToSend << clients[i].alias<<clients[i].pos.x<< clients[i].pos.y;
						packetescriticos[pckCounter] = pckToSend;
						pckCounter++;
						//std::cout << "Al jugador con alias " << clients[i].alias << " le envio el jugador con alias " << temp.alias << std::endl;
						if (sock->send(pckToSend, temp.ip, temp.port) != sf::Socket::Done)
						{
							std::cout << "errrrrrrrrrrrrrrrrrrrrrrrrrrrrrrror";
						}
						pckToSend.clear();
					}
				}
				//NOTWELCOME
				else 
				{
					commandToSend = NOTWELCOME;
					pckToSend << pckCounter << commandToSend;
					packetescriticos[pckCounter] = pckToSend;
					pckCounter++;
					//std::cout << "Al jugador con alias " << clients[i].alias << " le envio el jugador con alias " << temp.alias << std::endl;
					if (sock->send(pckToSend, temp.ip, temp.port) != sf::Socket::Done)
					{
						std::cout << "errrrrrrrrrrrrrrrrrrrrrrrrrrrrrrror";
					}
					temp.alias = "";
					pckToSend.clear();
				}				
				pckToReceive.clear();
				commandToReceive = -1;				
				break;

			case ACKN:
			{

				pckToReceive >> id_pck;
				std::string str;
				pckToReceive >> str;
				std::cout << "Aqui estoy" << std::endl;
				std::cout <<"ACK del cliente con alias: "<<str<< " El idpck es: " << id_pck << std::endl;
				toErase.push_back(id_pck);

				pckToReceive.clear();				
				id_pck = -1;
				commandToReceive = -1;
			

			break;
			}
			case MOVEMENT:
			{
				pckToReceive >> idMove;
				pckToReceive >> DeltaX;
				pckToReceive >> AcumX;
				pckToReceive >> AcumY;
				std::cout << "idMove: " << " " << idMove << "DeltaXs:  " <<" "<< DeltaX << "aBSX:  " << " " << AcumX << "ABSY :  " << " " << AcumY << std::endl;
				Movements.push_back(accum);

				
				std::cout << "Me estan enviando movimiento" << std::endl;
				commandToReceive = -1;
				break;
			}
			case PINGACKN:
			{
				pckToReceive >> id_pck;
				std::string str;
				pckToReceive >> str;
				std::cout << "Aqui estoy" << std::endl;
				std::cout << "ACK del cliente con alias: " << str << " El idpck es: " << id_pck << std::endl;
				
				for (size_t i = 0; i < clients.size(); i++)
				{
					clients[i].numPings = 0;

				}

				break;
			}
			default:
				break;
			}
		}
	}	
}

void  main()
{	

	sql::Driver* driver = sql::mysql::get_driver_instance();
	sf::UdpSocket sock;
	sf::UdpSocket::Status st = sock.bind(50000);

	std::map<int,sf::Packet> packetescriticos;
	std::vector<int> pckToErase;

	std::vector <clientProxy> Proxys;
	
	std::thread pckCriticosThread(&pckFunction, &sock, std::ref(Proxys), std::ref(packetescriticos), std::ref(pckToErase));
	pckCriticosThread.detach();

	std::thread pingThread(&pingFunction, &sock, std::ref(Proxys));
	pingThread.detach();

	std::thread receiveThread(&receiveFunction, &sock, std::ref(Proxys), std::ref(packetescriticos), std::ref(pckToErase));
	receiveThread.detach();

	//std::map<int, clientProxy> mapClients;

	if (st == sf::UdpSocket::Status::Done)
	{
		while (true) 
		{			
			
		}
	}


}