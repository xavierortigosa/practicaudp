#include <iostream>
#include <thread>
#include <SFML\Graphics.hpp>
#include <PlayerInfo.h>


void acknowledgePing(sf::UdpSocket* sock, int pck_id, std::string name)
{
	sf::Packet pck_ack;
	sf::IpAddress ip = sf::IpAddress::getLocalAddress();
	std::string str;
	pck_ack << PINGACKN << pck_id << name;

	if (sock->send(pck_ack, ip, 50000) != sf::Socket::Done)
	{
		std::cout << "errrrrrrrrrrrrrrrrrrrrrrrrrrrrrrror";
	}
	pck_ack.clear();
}

void acknowledge(sf::UdpSocket* sock, int pck_id, std::string name)
{
	sf::Packet pck_ack;
	sf::IpAddress ip = sf::IpAddress::getLocalAddress();
	std::string str;
	pck_ack<<ACKN<<pck_id << name;

	if (sock->send(pck_ack, ip, 50000) != sf::Socket::Done)
	{
		std::cout << "errrrrrrrrrrrrrrrrrrrrrrrrrrrrrrror";
	}
	pck_ack.clear();
}

void receiveThread(sf::UdpSocket* sock, std::vector<players> &_players)
{
	sf::Packet pckToReceive;
	sf::Packet pckToSend;

	int commandToReceive;
	int commandToSend;

	int pck_id;

	std::string alias;
	sf::IpAddress senderIP;
	unsigned short senderPort;
	std::string str;

	while (true)
	{		
		sf::UdpSocket::Status status = sock->receive(pckToReceive, senderIP, senderPort);
		players newPlayer;

		if (status == sf::UdpSocket::Status::Done)
		{
					
			pckToReceive >> commandToReceive;
			std::cout << "cmd" << commandToReceive << std::endl;
			
			switch (commandToReceive)
			{
			case WELCOME:				
				pckToReceive >> pck_id;
				pckToReceive >> newPlayer.alias;
				pckToReceive >> newPlayer.pos.x;
				pckToReceive >> newPlayer.pos.y;
				pckToReceive.clear();
				_players.push_back(newPlayer);
				std::cout << "Players size is : " << _players.size()<< std::endl;
				std::cout << "WELCOME: " << newPlayer.alias<<" pos X: "<<newPlayer.pos.x << "y: " << newPlayer.pos.y << std::endl;
				acknowledge(sock, pck_id, alias);
				commandToReceive = -1;
				pck_id = -1;
				pckToReceive.clear();
				break;
			case NOTWELCOME:
				pckToReceive >> pck_id;
				pckToReceive >> newPlayer.alias;
				pckToReceive >> newPlayer.pos.x;
				pckToReceive >> newPlayer.pos.y;
				pckToReceive.clear();
				std::cout << "YOU SHALL NOT PASS"<< std::endl;
				acknowledge(sock, pck_id, alias);
				commandToReceive = -1;
				pck_id = -1;
				pckToReceive.clear();
				exit(0);
				break;

			case PING:
				pckToReceive >> pck_id;
				pckToReceive >> newPlayer.alias;
				pckToReceive >> newPlayer.pos.x;
				pckToReceive >> newPlayer.pos.y;
				pckToReceive.clear();
				std::cout << "YOU SHALL NOT PASS" << std::endl;
				acknowledgePing(sock, pck_id, alias);
				commandToReceive = -1;
				pck_id = -1;
				pckToReceive.clear();
				exit(0);
				break;

			default:
				break;
			}
		}
	}
}

void drawGrid(sf::RenderWindow &window)
{
	sf::RectangleShape rectangle;
	rectangle.setFillColor(sf::Color(255, 0, 0));
	rectangle.setSize(sf::Vector2f(8, 8));
	bool color = true;

	window.clear();
	color = true;
	for (int i = 0; i < 63; i++)
	{
		for (int j = 0; j < 63; j++)
		{
			if (color)
			{
				rectangle.setFillColor(sf::Color(0, 0, 0));
				color = false;
			}
			else if (!color)
			{
				rectangle.setFillColor(sf::Color(155, 0, 0));
				color = true;
			}
			rectangle.setPosition(sf::Vector2f(8 * j, 8 * i));
			window.draw(rectangle);
		}
	}
	
}

void drawPlayers(sf::RenderWindow &window, std::vector<players> &_Clientes )
{
	//std::cout << _Clientes.size() << std::endl;

	sf::CircleShape shape;
	shape.setFillColor(sf::Color(255, 0, 0));
	shape.setRadius(4);
	for (int i = 0; i < _Clientes.size(); i++)
	{
		if(i==0) shape.setFillColor(sf::Color(0, 0, 255));
		else shape.setFillColor(sf::Color(0, 255, 255));
		
		shape.setPosition(sf::Vector2f(8 * _Clientes[i].pos.x, 8 * _Clientes[i].pos.y));
		window.draw(shape);
	}
	window.display();
}
struct points {
	int posy, posx;
};


void main() 
{

	AccumMovement ac;
	ac.idMove = 0; ac.DeltaXs = 0; ac.AbsoluteX = 0; ac.AbsoluteY = 0;
	srand(time(NULL));
	std::vector<AccumMovement>Movements;
	std::string str;
	std::cout << "Introduce un alias: ";
	std::cin >> str;

	int poX= (rand() % 63) ;
	int poY= (rand() % 63) ;
	
	sf::UdpSocket sock;
	sf::Packet pck;
	pck<< HELLO <<- 1 << str<<poX<<poY;
	sf::IpAddress a = sf::IpAddress::getLocalAddress();

	//Creo vector de clientes e inicializo el propio cliente
	std::vector<players> Clientes;
	Clientes.push_back({ { (poX),(poY) }, { str } });


	if (sock.send(pck, a, 50000) != sf::Socket::Done) 
	{
		std::cout << "errrrrrrrrrrrrrrrrrrrrrrrrrrrrrrror";
	}

	std::thread thread(&receiveThread, &sock, std::ref(Clientes));
	thread.detach();

	sf::RenderWindow window(sf::VideoMode(504, 504), "Ejemplo tablero");
	std::cout << "x: " << Clientes[0].pos.x << " " << "y: " << Clientes[0].pos.y << std::endl;
	
	std::vector<points>Points;
	sf::Vector2f pos;
	sf::Clock deltaClock;
	sf::Time prevTime = deltaClock.getElapsedTime();
	pos.x = Clientes[0].pos.x;
	pos.y = Clientes[0].pos.y;

	while(window.isOpen())
	{
		sf::Time elapsedTime = deltaClock.getElapsedTime() - prevTime;
	
		//std::cout << elapsedTime.asMilliseconds() << std::endl;
		sf::Event event;
		while (window.pollEvent(event))
		{
			switch (event.type)
			{
			case sf::Event::KeyPressed:
				if (event.key.code == sf::Keyboard::W) {
					std::cout << "W" << std::endl;
					
					ac.idMove += 1;
					ac.DeltaXs = elapsedTime.asMilliseconds();
					pos.y = pos.y - 1;
					ac.AbsoluteY = pos.y;
					ac.AbsoluteX = pos.x;
					std::cout <<"absX :"<< ac.AbsoluteX << std::endl;
					std::cout << "absY :" << ac.AbsoluteY << std::endl;
					Movements.push_back(ac);
					//acknowledge(&sock, 0, "a");
				}
				if (event.key.code == sf::Keyboard::A) {
					std::cout << "A" << std::endl;
					//Clientes[0].pos.x -= 1;
					ac.idMove += 1;
					ac.DeltaXs = elapsedTime.asMilliseconds();
					pos.x = pos.x - 1;
					ac.AbsoluteX = pos.x;
					ac.AbsoluteY = pos.y;
					std::cout << "absX :" << ac.AbsoluteX << std::endl;
					std::cout << "absY :" << ac.AbsoluteY << std::endl;
					Movements.push_back(ac);
				}
				if (event.key.code == sf::Keyboard::S) {
					std::cout << "S" << std::endl;
					//Clientes[0].pos.y += 1;
					ac.idMove += 1;
					ac.DeltaXs = elapsedTime.asMilliseconds();
					pos.y = pos.y + 1;
					ac.AbsoluteY = pos.y;
					ac.AbsoluteX = pos.x;
					std::cout << "absX :" << ac.AbsoluteX << std::endl;
					std::cout << "absY :" << ac.AbsoluteY << std::endl;
					Movements.push_back(ac);
				}
				if (event.key.code == sf::Keyboard::D) {
					std::cout << "D" << std::endl;
					//Clientes[0].pos.x += 1;
					ac.idMove += 1;
					ac.DeltaXs = elapsedTime.asMilliseconds();
					pos.x = pos.x + 1;
					ac.AbsoluteX= pos.x;
					ac.AbsoluteY = pos.y;
					std::cout << "absX :" << ac.AbsoluteX << std::endl;
					std::cout << "absY :" << ac.AbsoluteY << std::endl;
					Movements.push_back(ac);
				}
			default:
				break;
			}


		}

		
		drawPlayers(window, std::ref(Clientes));
		drawGrid(window);
		//drawPlayers(window, std::ref(Clientes));
		
		
		if (elapsedTime > sf::milliseconds(2000) && Movements.size() !=0)
		{
			std::cout << "SEND" << std::endl;
			pck << MOVEMENT;
			for (int i = 0; i < Movements.size(); i++) 
			{
				//Rellenamos el pck con los datos del vector de movimientos accumulados. 
				pck << Movements[i].idMove << Movements[i].DeltaXs << Movements[i].AbsoluteX<<Movements[i].AbsoluteY;
				std::cout <<"idMove: "<< Movements[i].idMove << "deltaXs: " << Movements[i].DeltaXs << "AbsX: " << Movements[i].AbsoluteX << "AbsY: " << Movements[i].AbsoluteY;
				// enviar informacion
				if (sock.send(pck, a, 50000) != sf::Socket::Done)
				{
					std::cout << "errrrrrrrrrrrrrrrrrrrrrrrrrrrrrrror";
				}
				pck.clear();
			}
			prevTime = deltaClock.getElapsedTime(); // reseteamos el tiempo 
			Movements.clear(); //Vaciamos el vector
		}
	}
}
