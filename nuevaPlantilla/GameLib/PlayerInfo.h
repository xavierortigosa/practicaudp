#pragma once
#include <SFML\Graphics.hpp>
#include <SFML\Network.hpp>

enum msgType { HELLO, WELCOME,NOTWELCOME, NEWPLAYER, ACKN, MOVEMENT, PING, PINGACKN};
struct clientProxy
{
	sf::Vector2i pos;
	sf::IpAddress ip;
	unsigned short port;
	std::string alias;
	int numPings;
};

struct players
{
	sf::Vector2i pos;
	std::string alias;
};

class PlayerInfo
{
	std::string name;
	sf::Vector2i position;
	int lives;
public:
	PlayerInfo();
	~PlayerInfo();
};

struct AccumMovement {
	int idMove;
	int  DeltaXs;
	int AbsoluteX;
	int AbsoluteY;
};